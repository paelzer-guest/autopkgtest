#!/bin/sh

# autopkgtest-build-qemu is part of autopkgtest
# autopkgtest is a tool for testing Debian binary packages
#
# Copyright (C) Antonio Terceiro <terceiro@debian.org>.
#
# Build a QEMU image for using with autopkgtest
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
#
# See the file CREDITS for a full list of credits information (often
# installed as /usr/share/doc/autopkgtest/CREDITS).

set -eu


if [ $# -lt 2 -o $# -gt 5 ]; then
    echo "usage: $0 <release> <image> [<mirror>] [<architecture>] [<script>]"
    exit 1
fi

if ! which vmdb2 > /dev/null; then
  echo "E: vmdb2 not found. This script requires vmdb2 to be installed"
  exit 2
fi


release="$1"
image="$2"
mirror="${3:-http://deb.debian.org/debian}"
architecture="${4:-}"
if [ -z "$architecture" ]; then
    architecture=$(dpkg --print-architecture)
fi
user_script="${5:-/dev/null}"


# detect apt proxy
# support backwards compatible env var too
AUTOPKGTEST_APT_PROXY=${AUTOPKGTEST_APT_PROXY:-${ADT_APT_PROXY:-}}
if [ -z "$AUTOPKGTEST_APT_PROXY" ]; then
    RES=`apt-config shell proxy Acquire::http::Proxy`
    if [ -n "$RES" ]; then
        eval $RES
    else
        RES=`apt-config shell proxy_cmd Acquire::http::Proxy-Auto-Detect`
        eval $RES
        if [ -n "${proxy_cmd:-}" ]; then
            proxy=`$proxy_cmd`
        fi
    fi
    if echo "${proxy:-}" | egrep -q '(localhost|127\.0\.0\.[0-9]*)'; then
        # set http_proxy for the initial debootstrap
        export http_proxy="$proxy"

        # translate proxy address to one that can be accessed from the
        # running VM
        AUTOPKGTEST_APT_PROXY=$(echo "$proxy" | sed -r "s#localhost|127\.0\.0\.[0-9]*#10.0.2.2#")
        if [ -n "$AUTOPKGTEST_APT_PROXY" ]; then
            echo "Detected local apt proxy, using $AUTOPKGTEST_APT_PROXY as virtual machine proxy"
        fi
    elif [ -n "${proxy:-}" ]; then
        AUTOPKGTEST_APT_PROXY="$proxy"
        echo "Using $AUTOPKGTEST_APT_PROXY as container proxy"
        # set http_proxy for the initial debootstrap
        export http_proxy="$proxy"
    fi
fi


script=/bin/true
for s in $(dirname $(dirname "$0"))/setup-commands/setup-testbed \
              /usr/share/autopkgtest/setup-commands/setup-testbed; do
    if [ -r "$s" ]; then
        script="$s"
        echo "Using customization script $script ..."
        break
    fi
done


case "$mirror" in
  *ubuntu*)
    kernel=linux-image-virtual
    ;;
  *)
    case "$architecture" in
      (armhf)
        kernel=linux-image-armmp
        ;;
      (hppa)
        kernel=linux-image-parisc
        ;;
      (i386)
        case "$release" in
          (jessie)
            kernel=linux-image-586
            ;;
          (*)
            kernel=linux-image-686
            ;;
        esac
        ;;
      (ppc64)
        kernel=linux-image-powerpc64
        ;;
      (*)
        kernel="linux-image-$architecture"
        ;;
    esac
    ;;
esac

vmdb2_config=$(mktemp)
trap "rm -rf $vmdb2_config" INT TERM EXIT
cat > "$vmdb2_config" <<EOF
steps:
  - mkimg: "{{ image }}"
    size: 20G

  - mklabel: msdos
    device: "{{ image }}"

  - mkpart: primary
    device: "{{ image }}"
    start: 0%
    end: 100%
    part-tag: root-part

  - mkfs: ext4
    partition: root-part

  - mount: root-part
    fs-tag: root-fs

  - debootstrap: $release
    mirror: $mirror
    target: root-fs

  - apt: install
    packages:
      - $kernel
      - ifupdown
    fs-tag: root-fs

  - chroot: root-fs
    shell: |
      passwd --delete root
      useradd --home-dir /home/user --create-home user
      passwd --delete user
      echo host > /etc/hostname

  - grub: bios
    root-fs: root-fs
    root-part: root-part
    device: "{{ image }}"
    console: serial

  - mount-virtual-filesystems: root-fs

  - shell: '$script \$ROOT'
    root-fs: root-fs

  - shell: 'chroot \$ROOT sh < $user_script'
    root-fs: root-fs

EOF

vmdb2 \
    --verbose \
    --image="$image".raw \
    "$vmdb2_config"

qemu-img convert -O qcow2 "$image".raw  "$image".new

rm -f "$image".raw

# replace a potentially existing image as atomically as possible
mv "$image".new "$image"
